#include "engine.h"

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <ranges>
#include <utility>

namespace
{

void trade(TradeMatching::Order& a, TradeMatching::Order& b, unsigned int price, TradeMatching::Trades& result)
{
	assert(a.type != b.type);

	const unsigned int quantity = std::min(a.quantity, b.quantity);

	a.quantity -= quantity;
	b.quantity -= quantity;
	result.emplace_back(a.traderIdentifier, a.type, quantity, price);
	result.emplace_back(b.traderIdentifier, b.type, quantity, price);
}

[[nodiscard]] auto trade(TradeMatching::Order& aggressor, std::ranges::bidirectional_range auto&& potentialTrades)
{
	using std::ranges::begin;
	using std::ranges::end;
	using std::ranges::advance;

	struct Result
	{
		TradeMatching::Trades trades;
		std::ranges::iterator_t<decltype(potentialTrades)> firstOrderWithNonZeroQuantity;
	};

	Result result{{}, begin(potentialTrades)};

	std::ptrdiff_t tradedCount{0};
	for (auto& [_, potentialTrade] : potentialTrades)
	{
		assert(aggressor.type != potentialTrade.type);

		if (0 == aggressor.quantity)
			break;

		trade(aggressor, potentialTrade, potentialTrade.price, result.trades);
		++tradedCount;
	}

	if (!tradedCount)
		return result;

	advance(result.firstOrderWithNonZeroQuantity, tradedCount - 1, end(potentialTrades));
	if (0 == result.firstOrderWithNonZeroQuantity->second.quantity)
		advance(result.firstOrderWithNonZeroQuantity, 1, end(potentialTrades));

	return result;
}

[[nodiscard]] TradeMatching::Trades match(TradeMatching::Order aggressor, auto& aggressorSide, auto& oppositeSide)
{
	const unsigned int worstPriceToConsider = aggressor.price;

	const std::ranges::subrange potentialTradesView{oppositeSide.begin(), oppositeSide.upper_bound(worstPriceToConsider)};
	auto [trades, firstOrderWithNonZeroQuantity] = trade(aggressor, potentialTradesView);

	oppositeSide.erase(potentialTradesView.begin(), firstOrderWithNonZeroQuantity);

	if (0 != aggressor.quantity)
		aggressorSide.emplace(worstPriceToConsider, std::move(aggressor));

	return std::move(trades);
}

} // namespace

namespace TradeMatching
{

Trades Engine::placeOrder(Order aggressor)
{
	switch (aggressor.type)
	{
		using enum Order::Type;
	case Buying:
		return match(std::move(aggressor), buyingRestingOrders, sellingRestingOrders);
	case Selling:
		return match(std::move(aggressor), sellingRestingOrders, buyingRestingOrders);
	}
	return {};
}

} // namespace TradeMatching
