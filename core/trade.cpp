#include "trade.h"

#include <algorithm>
#include <cassert>
#include <ranges>

namespace
{

[[nodiscard]] bool compareTrades(const TradeMatching::Trade& a, const TradeMatching::Trade& b)
{
	if (a.traderIdentifier != b.traderIdentifier)
		return a.traderIdentifier < b.traderIdentifier;
	if (a.type != b.type)
		return TradeMatching::Trade::Type::Selling == a.type;
	if (a.price != b.price)
		return a.price < b.price;
	return false;
}

[[nodiscard]] bool shouldCombineTrades(const TradeMatching::Trade& a, const TradeMatching::Trade& b)
{
	return !compareTrades(a, b) && !compareTrades(b, a);
}

} // namespace

namespace TradeMatching
{

Trades getSortedCombinedTrades(Trades trades)
{
	std::ranges::sort(trades, compareTrades);

	Trades result{};
	result.reserve(trades.size());

#if __cpp_lib_ranges_chunk_by
	for (auto tradesToCombine : trades | std::views::chunk_by(shouldCombineTrades))
	{
		assert(!tradesToCombine.empty());

		result.push_back(std::move(tradesToCombine.front()));
		for (const auto& tradeToCombine : tradesToCombine | std::views::drop(1))
			result.back().quantity += tradeToCombine.quantity;
	}
#else
	if (trades.empty())
		return result;

	result.push_back(std::move(trades.front()));
	for (auto& trade : trades | std::views::drop(1))
	{
		if (shouldCombineTrades(result.back(), trade))
			result.back().quantity += trade.quantity;
		else
			result.push_back(std::move(trade));
	}
#endif

	return result;
}

} // namespace TradeMatching
