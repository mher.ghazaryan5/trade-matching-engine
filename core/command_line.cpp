#include "command_line.h"

#include <cassert>
#include <iostream>
#include <ranges>
#include <regex>
#include <sstream>
#include <utility>

namespace
{

[[nodiscard]] constexpr TradeMatching::Order::Type getOrderType(char side)
{
	assert('B' == side || 'S' == side);

	switch (side)
	{
	case 'B':
		return TradeMatching::Order::Type::Buying;
	case 'S':
		return TradeMatching::Order::Type::Selling;
	}
	return TradeMatching::Order::Type::Buying;
}

[[nodiscard]] constexpr char getTradeTypeSign(TradeMatching::Trade::Type tradeType)
{
	switch (tradeType)
	{
		using enum TradeMatching::Trade::Type;
	case Buying:
		return '+';
	case Selling:
		return '-';
	}
	return ' ';
}

std::ostream& printTrade(std::ostream& ostream, const TradeMatching::Trade& trade)
{
	return ostream << trade.traderIdentifier << getTradeTypeSign(trade.type) << trade.quantity << '@' << trade.price;
}

} // namespace

namespace TradeMatching::CommandLine
{

OrderParsingError::OrderParsingError(const std::string& string)
    : std::runtime_error("couldn't parse \"" + string + "\" as an order")
{
}

Order parseOrder(const std::string& string)
{
	// language=RegExp
	static const std::regex pattern{R"(\s*[[:alnum:]]+\s+[BS]\s+(?:0|[1-9]\d*)\s+(?:0|[1-9]\d*)\s*)", std::regex_constants::optimize};
	if (std::smatch match; !std::regex_match(string, match, pattern))
		throw OrderParsingError{string};

	std::istringstream iss{string};

	std::string traderIdentifier{};
	char side{};
	unsigned int quantity{};
	unsigned int price{};
	iss >> traderIdentifier >> side >> quantity >> price;
	assert('B' == side || 'S' == side);

	return {std::move(traderIdentifier),
	        getOrderType(side),
	        quantity,
	        price};
}

std::ostream& printTrades(std::ostream& ostream, const Trades& trades)
{
	if (trades.empty())
		return ostream;

	printTrade(ostream, trades.front());
	for (const TradeMatching::Trade& trade : trades | std::views::drop(1))
		printTrade(ostream << ' ', trade);

	return ostream;
}

} // namespace TradeMatching::CommandLine
