#pragma once

#include "order.h"
#include "trade.h"

#include <functional>
#include <map>
#include <vector>

namespace TradeMatching
{

class Engine
{
public:
	/**
	 * Places an order into the market and returns the trades that take place
	 *
	 * @details Orders that exist in the market are called resting orders.\n
	 * The aggressor is matched against resting orders of the opposite side (type) with the best price first.\n
	 * If there are many resting orders at the best price then resting orders are taken from oldest to newest.\n
	 * Matching continues, until all the aggressor quantity is matched or the requested price no longer matches opposite orders.\n
	 * If there are no opposite orders with overlapping prices in the market then the aggressor rests.\n
	 *
	 * @param aggressor the placed order
	 * @return the trades that take place
	 */
	[[nodiscard]] Trades placeOrder(Order aggressor);

private:
	std::multimap<unsigned int, Order, std::greater<>> buyingRestingOrders{};
	std::multimap<unsigned int, Order, std::less<>> sellingRestingOrders{};
};

} // namespace TradeMatching
