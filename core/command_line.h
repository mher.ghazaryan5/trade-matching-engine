#pragma once

#include "order.h"
#include "trade.h"

#include <iosfwd>
#include <stdexcept>
#include <string>

namespace TradeMatching::CommandLine
{

class OrderParsingError : public std::runtime_error
{
public:
	explicit OrderParsingError(const std::string& string);
};

/**
 * Parses string as an Order
 *
 * @details The string should have the following syntax:\n
 * \<Trader Identifier\> \<Side\> \<Quantity\> \<Price\>\n
 * \n
 * where\n
 * \<Trader Identifier\> is an alpha-numeric string\n
 * \<Side\> is a single char: 'B' if this is a buy request, and 'S' if this is a sell request\n
 * \<Quantity\> is an integer size of a request\n
 * \<Price\> is an integer price of request\n
 *
 * @param string the string to parse
 * @return the parsed order
 * @exception OrderParsingError on invalid string
 */
[[nodiscard]] Order parseOrder(const std::string& string);

/**
 * Prints trades to an output stream
 *
 * @details The trades are printed on a single line with the following format:\n
 * \<Trader Identifier\>\<Sign\>\<Quantity\>\@\<Price\>\n
 * \n
 * where\n
 * \<Trader Identifier\> is the trader identifier\n
 * \<Sign\> is "+" for a buy and "-" for a sell\n
 * \<Quantity\> is the quantity traded\n
 * \<Price\> is the price at which the trade took place\n
 * \n
 * The trade quantities do not have to add-up to 0
 *
 * @param ostream the output stream to print to
 * @param trades the trades to print
 * @return ostream
 */
std::ostream& printTrades(std::ostream& ostream, const Trades& trades);

} // namespace TradeMatching::CommandLine
