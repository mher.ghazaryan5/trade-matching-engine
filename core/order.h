#pragma once

#include <string>

namespace TradeMatching
{

struct Order
{
	enum class Type
	{
		Buying,
		Selling
	};

	std::string traderIdentifier;
	Type type;
	unsigned int quantity{0};
	unsigned int price{0};

	Order(std::string traderIdentifier, Type type, unsigned int quantity, unsigned int price);

	[[nodiscard]] bool operator==(const Order&) const = default;
	[[nodiscard]] bool operator!=(const Order&) const = default;
};

} // namespace TradeMatching
