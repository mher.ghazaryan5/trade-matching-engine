#include "order.h"

#include <utility>

namespace TradeMatching
{

Order::Order(std::string traderIdentifier, Order::Type type, unsigned int quantity, unsigned int price)
    : traderIdentifier(std::move(traderIdentifier))
    , type(type)
    , quantity(quantity)
    , price(price)
{
}

} // namespace TradeMatching