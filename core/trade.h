#pragma once

#include "order.h"

#include <vector>

namespace TradeMatching
{

using Trade = Order;
using Trades = std::vector<Trade>;

/**
 * Returns the sorted and combined trades
 *
 * @details Trades with the same trader, type and price are combined.\n
 * The trades are sorted by their trader, type and price in ascending order.\n
 * Selling trades come before buying trades with the same trader.
 *
 * @param trades the trades to sort and combine
 * @return the sorted and combined trades
 */
[[nodiscard]] Trades getSortedCombinedTrades(Trades trades);

} // namespace TradeMatching
