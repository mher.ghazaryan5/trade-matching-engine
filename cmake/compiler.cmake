set(gcc_like_cxx "$<COMPILE_LANG_AND_ID:CXX,ARMClang,AppleClang,Clang,GNU>")
set(msvc_cxx "$<COMPILE_LANG_AND_ID:CXX,MSVC>")

add_library(compiler_options INTERFACE)
target_compile_features(compiler_options INTERFACE cxx_std_23)
target_compile_options(
        compiler_options INTERFACE

        $<${gcc_like_cxx}:$<BUILD_INTERFACE:-Wall -Wextra -Wpedantic -pedantic-errors -Wconversion -Wsign-conversion -Warith-conversion -Werror>>
        $<${msvc_cxx}:$<BUILD_INTERFACE:-W4 -WX>>
)
