# Trade Matching Engine

## Prerequisites

* CMake (>= 3.24)
* C++20 compliant compiler
    * GCC (>= 12)
    * MSVC (>= 19.31)

Note, that GCC 12 is not fully C++20 compliant, however it is enough for building this project.

## Getting sources

Use `--recurse-submodules`(`--recursive` for older Git versions) option for `git clone`, or run the following command
after cloning without the option:

```shell
git submodule update --init --recursive
```

## Dependencies

All the dependencies are managed by vcpkg and will be downloaded automatically during the CMake configuration step.

This project depends on the following packages:

* GoogleTest

## Using an IDE

Open the project folder with an editor of your choice that supports CMake projects (CLion, Visual Studio, etc.).

## Building from the command line

Consider using an IDE in general to make working with the project easier. See the previous section for more information.

From the project root folder run

```shell
cmake -DCMAKE_BUILD_TYPE=Release -S . -B cmake-build-release
cmake --build cmake-build-release
```

Note, that there might be need to specify a generator for some platforms.

Also note, that if you're using MinGW, there might be need to set environment
variables `VCPKG_DEFAULT_HOST_TRIPLET=x64-mingw-dynamic` and `VCPKG_DEFAULT_TRIPLET=x64-mingw-dynamic`, and replace the
commands above with

```shell
cmake -DCMAKE_BUILD_TYPE=Release -S . -B cmake-build-release -DVCPKG_TARGET_TRIPLET=x64-mingw-dynamic
cmake --build cmake-build-release
```
