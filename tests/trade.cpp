#include "trade.h"

#include <core/trade.h>
#include <gtest/gtest.h>

TEST(GetSortedCombinedTrades, Empty)
{
	TradeMatching::Trades input{};
	static const TradeMatching::Trades expected{};

	const TradeMatching::Trades result = TradeMatching::getSortedCombinedTrades(std::move(input));
	ASSERT_EQ(expected, result);
}

TEST(GetSortedCombinedTrades, SingleBuyingTrade)
{
	TradeMatching::Trades input{
	    {"T1", TradeMatching::Trade::Type::Buying, 5, 30},
	};
	static const TradeMatching::Trades expected{
	    {"T1", TradeMatching::Trade::Type::Buying, 5, 30},
	};

	const TradeMatching::Trades result = TradeMatching::getSortedCombinedTrades(std::move(input));
	ASSERT_EQ(expected, result);
}

TEST(GetSortedCombinedTrades, SingleSellingTrade)
{
	TradeMatching::Trades input{
	    {"T2", TradeMatching::Trade::Type::Selling, 5, 70},
	};
	static const TradeMatching::Trades expected{
	    {"T2", TradeMatching::Trade::Type::Selling, 5, 70},
	};

	const TradeMatching::Trades result = TradeMatching::getSortedCombinedTrades(std::move(input));
	ASSERT_EQ(expected, result);
}

TEST(GetSortedCombinedTrades, MultipleTrades)
{
	TradeMatching::Trades input{
	    {"T1", TradeMatching::Trade::Type::Buying, 1, 120},
	    {"T2", TradeMatching::Trade::Type::Buying, 4, 100},
	    {"T2", TradeMatching::Trade::Type::Buying, 5, 100},
	    {"T2", TradeMatching::Trade::Type::Buying, 6, 70},
	    {"T2", TradeMatching::Trade::Type::Selling, 1, 100},
	    {"T1", TradeMatching::Trade::Type::Buying, 5, 120},
	};
	static const TradeMatching::Trades expected{
	    {"T1", TradeMatching::Trade::Type::Buying, 6, 120},
	    {"T2", TradeMatching::Trade::Type::Selling, 1, 100},
	    {"T2", TradeMatching::Trade::Type::Buying, 6, 70},
	    {"T2", TradeMatching::Trade::Type::Buying, 9, 100},
	};

	const TradeMatching::Trades result = TradeMatching::getSortedCombinedTrades(std::move(input));
	ASSERT_EQ(expected, result);
}
