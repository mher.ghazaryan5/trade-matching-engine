#include "order.h"

#include <core/command_line.h>
#include <core/order.h>
#include <core/trade.h>
#include <gtest/gtest.h>

#include <sstream>
#include <string>
#include <string_view>
#include <utility>

namespace
{

[[nodiscard]] std::string getTradesString(const TradeMatching::Trades& trades)
{
	std::ostringstream oss{};
	TradeMatching::CommandLine::printTrades(oss, trades);
	return oss.str();
}

} // namespace

namespace CommandLine
{

TEST(ParseOrder, BuyingOrder)
{
	static const std::string string = "T1 B 5 30";
	static const TradeMatching::Order expected{"T1", TradeMatching::Order::Type::Buying, 5, 30};

	const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string);
	ASSERT_EQ(expected, result);
}

TEST(ParseOrder, BuyingOrderWithSpaces)
{
	static const std::string string = "          T1     B       5     30         ";
	static const TradeMatching::Order expected{"T1", TradeMatching::Order::Type::Buying, 5, 30};

	const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string);
	ASSERT_EQ(expected, result);
}

TEST(ParseOrder, BuyingOrderWithZeroQuantityAndPrice)
{
	static const std::string string = "T1 B 0 0";
	static const TradeMatching::Order expected{"T1", TradeMatching::Order::Type::Buying, 0, 0};

	const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string);
	ASSERT_EQ(expected, result);
}

TEST(ParseOrder, SellingOrder)
{
	static const std::string string = "T2 S 5 70";
	static const TradeMatching::Order expected{"T2", TradeMatching::Order::Type::Selling, 5, 70};

	const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string);
	ASSERT_EQ(expected, result);
}

TEST(ParseOrder, SellingOrderWithSpaces)
{
	static const std::string string = "        T2         S         5       70          ";
	static const TradeMatching::Order expected{"T2", TradeMatching::Order::Type::Selling, 5, 70};

	const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string);
	ASSERT_EQ(expected, result);
}

TEST(ParseOrder, SellingOrderWithZeroQuantityAndPrice)
{
	static const std::string string = "T2 S 0 0";
	static const TradeMatching::Order expected{"T2", TradeMatching::Order::Type::Selling, 0, 0};

	const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string);
	ASSERT_EQ(expected, result);
}

TEST(ParseOrder, EmptyOrder)
{
	static const std::string string{};

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(ParseOrder, OrderWithInvalidTrader)
{
	static const std::string string = "T_1 B 5 30";

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(ParseOrder, OrderWithInvalidSide)
{
	static const std::string string = "T1 A 5 30";

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(ParseOrder, OrderWithQuantityStartingWithZero)
{
	static const std::string string = "T1 B 05 30";

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(ParseOrder, OrderWithInvalidQuantity)
{
	static const std::string string = "T1 B 5A 30";

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(ParseOrder, OrderWithPriceStartingWithZero)
{
	static const std::string string = "T1 B 5 030";

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(ParseOrder, OrderWithInvalidPrice)
{
	static const std::string string = "T1 B 5 30A";

	ASSERT_THROW(const TradeMatching::Order result = TradeMatching::CommandLine::parseOrder(string),
	             TradeMatching::CommandLine::OrderParsingError);
}

TEST(PrintTrades, Empty)
{
	static const TradeMatching::Trades trades{};
	static constexpr std::string_view expected{};

	const std::string result = getTradesString(trades);
	ASSERT_EQ(expected, result);
}

TEST(PrintTrades, SingleBuyingTrade)
{
	static const TradeMatching::Trades trades{
	    {"T1", TradeMatching::Trade::Type::Buying, 5, 30},
	};
	static constexpr std::string_view expected = "T1+5@30";

	const std::string result = getTradesString(trades);
	ASSERT_EQ(expected, result);
}

TEST(PrintTrades, SingleSellingTrade)
{
	static const TradeMatching::Trades trades{
	    {"T2", TradeMatching::Trade::Type::Selling, 5, 70},
	};
	static constexpr std::string_view expected = "T2-5@70";

	const std::string result = getTradesString(trades);
	ASSERT_EQ(expected, result);
}

TEST(PrintTrades, MultipleTrades)
{
	static const TradeMatching::Trades trades{
	    {"T1", TradeMatching::Trade::Type::Buying, 1, 120},
	    {"T2", TradeMatching::Trade::Type::Buying, 4, 100},
	    {"T2", TradeMatching::Trade::Type::Buying, 5, 100},
	    {"T2", TradeMatching::Trade::Type::Buying, 6, 70},
	    {"T2", TradeMatching::Trade::Type::Selling, 1, 100},
	    {"T1", TradeMatching::Trade::Type::Buying, 5, 120},
	};
	static constexpr std::string_view expected = "T1+1@120 T2+4@100 T2+5@100 T2+6@70 T2-1@100 T1+5@120";

	const std::string result = getTradesString(trades);
	ASSERT_EQ(expected, result);
}

} // namespace CommandLine
