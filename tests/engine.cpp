#include "order.h"
#include "trade.h"

#include <core/engine.h>
#include <core/order.h>
#include <core/trade.h>
#include <gtest/gtest.h>

TEST(EnginePlaceOrder, SingleBuyingOrder)
{
	TradeMatching::Engine engine{};

	static const TradeMatching::Trades expected{};
	const TradeMatching::Trades trades = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 30});
	ASSERT_EQ(expected, trades);
}

TEST(EnginePlaceOrder, SingleSellingOrder)
{
	TradeMatching::Engine engine{};

	static const TradeMatching::Trades expected{};
	const TradeMatching::Trades trades = engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 5, 70});
	ASSERT_EQ(expected, trades);
}

TEST(EnginePlaceOrder, MultipleBuyingOrders)
{
	TradeMatching::Engine engine{};

	static const TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 30});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T3", TradeMatching::Order::Type::Buying, 1, 40});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 1, 50});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 3, 60});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T8", TradeMatching::Order::Type::Buying, 10, 90});
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, MultipleSellingOrders)
{
	TradeMatching::Engine engine{};

	static const TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 5, 70});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T4", TradeMatching::Order::Type::Selling, 2, 60});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T5", TradeMatching::Order::Type::Selling, 3, 70});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T6", TradeMatching::Order::Type::Selling, 20, 80});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T7", TradeMatching::Order::Type::Selling, 1, 50});
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, SingleBuyingOrderAndSingleNonMatchingSellingOrder)
{
	TradeMatching::Engine engine{};

	static const TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 30});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 5, 70});
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, SingleSellingOrderAndSingleNonMatchingBuyingOrder)
{
	TradeMatching::Engine engine{};

	static const TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 5, 70});
	EXPECT_EQ(expected, result);

	result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 30});
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, SingleBuyingOrderAndSingleMatchingSellingOrder)
{
	TradeMatching::Engine engine{};

	TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 30});
	EXPECT_EQ(expected, result);

	expected = TradeMatching::getSortedCombinedTrades({
	    {"T1", TradeMatching::Order::Type::Buying, 5, 30},
	    {"T2", TradeMatching::Order::Type::Selling, 5, 30},
	});
	result = TradeMatching::getSortedCombinedTrades(engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 5, 30}));
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, SingleSellingOrderAndSingleMatchingBuyingOrder)
{
	TradeMatching::Engine engine{};

	TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 5, 70});
	EXPECT_EQ(expected, result);

	expected = TradeMatching::getSortedCombinedTrades({
	    {"T2", TradeMatching::Order::Type::Selling, 5, 70},
	    {"T1", TradeMatching::Order::Type::Buying, 5, 70},
	});
	result = TradeMatching::getSortedCombinedTrades(engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 70}));
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, SingleBuyingOrderAndMultipleMatchingSellingOrders)
{
	TradeMatching::Engine engine{};

	TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T1", TradeMatching::Order::Type::Buying, 5, 50});
	EXPECT_EQ(expected, result);

	expected = TradeMatching::getSortedCombinedTrades({
	    {"T1", TradeMatching::Order::Type::Buying, 2, 50},
	    {"T2", TradeMatching::Order::Type::Selling, 2, 50},
	});
	result = TradeMatching::getSortedCombinedTrades(engine.placeOrder({"T2", TradeMatching::Order::Type::Selling, 2, 40}));
	EXPECT_EQ(expected, result);

	expected = TradeMatching::getSortedCombinedTrades({
	    {"T1", TradeMatching::Order::Type::Buying, 1, 50},
	    {"T3", TradeMatching::Order::Type::Selling, 1, 50},
	});
	result = TradeMatching::getSortedCombinedTrades(engine.placeOrder({"T3", TradeMatching::Order::Type::Selling, 1, 50}));
	EXPECT_EQ(expected, result);
}

TEST(EnginePlaceOrder, SingleSellingOrderAndMultipleMatchingBuyingOrders)
{
	TradeMatching::Engine engine{};

	TradeMatching::Trades expected{};
	TradeMatching::Trades result = engine.placeOrder({"T1", TradeMatching::Order::Type::Selling, 5, 50});
	EXPECT_EQ(expected, result);

	expected = TradeMatching::getSortedCombinedTrades({
	    {"T1", TradeMatching::Order::Type::Selling, 2, 50},
	    {"T2", TradeMatching::Order::Type::Buying, 2, 50},
	});
	result = TradeMatching::getSortedCombinedTrades(engine.placeOrder({"T2", TradeMatching::Order::Type::Buying, 2, 60}));
	EXPECT_EQ(expected, result);

	expected = TradeMatching::getSortedCombinedTrades({
	    {"T1", TradeMatching::Order::Type::Selling, 1, 50},
	    {"T3", TradeMatching::Order::Type::Buying, 1, 50},
	});
	result = TradeMatching::getSortedCombinedTrades(engine.placeOrder({"T3", TradeMatching::Order::Type::Buying, 1, 50}));
	EXPECT_EQ(expected, result);
}
