#include "order.h"

#include <iostream>
#include <string_view>

namespace
{

[[nodiscard]] constexpr std::string_view getOrderTypeString(TradeMatching::Order::Type orderType)
{
	switch (orderType)
	{
		using enum TradeMatching::Order::Type;
	case Buying:
		return "Buying";
	case Selling:
		return "Selling";
	}
	return "";
}

} // namespace

namespace TradeMatching
{

void PrintTo(const TradeMatching::Order& order, std::ostream* ostream)
{
	*ostream << "{.traderIdentifier = " << order.traderIdentifier
	         << ", .type = " << getOrderTypeString(order.type)
	         << ", .quantity = " << order.quantity
	         << ", .price = " << order.price
	         << "}";
}

} // namespace TradeMatching
