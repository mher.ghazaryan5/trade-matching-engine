#pragma once

#include <core/order.h>

#include <iosfwd>

namespace TradeMatching
{

void PrintTo(const Order& order, std::ostream* ostream);

} // namespace TradeMatching
