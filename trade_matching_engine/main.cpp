#include <core/core.h>

#include <iostream>
#include <string>
#include <utility>

int main()
try
{
	TradeMatching::Engine engine{};
	for (std::string line; std::getline(std::cin, line);)
	{
		if (line.empty())
			continue;

		TradeMatching::Order order = TradeMatching::CommandLine::parseOrder(line);
		TradeMatching::Trades trades = engine.placeOrder(std::move(order));
		trades = TradeMatching::getSortedCombinedTrades(std::move(trades));
		TradeMatching::CommandLine::printTrades(std::cout, trades) << '\n';
	}
	return 0;
}
catch (const std::exception& e)
{
	std::cerr << "error: " << e.what() << '\n';
	return 1;
}
catch (...)
{
	std::cerr << "unknown error\n";
	return 1;
}
